﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoEun.Common
{
    class GradeA
    {
        public String Size {get; set;}
        public int Count { get; set; }

        public GradeA(String size) 
        {
            this.Size = size;
            this.Count = 0;
        }
    }
}
