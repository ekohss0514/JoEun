﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoEun.Common
{
    class Menu
    {
        public String Title {get; set;}
        public String MenuCode { get; set; }
        public String Description { get; set; }

        public Menu(String title, String menuCode, String description) 
        {
            this.Title = title;
            this.MenuCode = menuCode;
            this.Description = description;
        }
    }

    class MenuDataSource
    {
        private List<Menu> _Items;

        public List<Menu> getMenuList()
        {
            _Items = new List<Menu>();

            Menu menu1 = new Menu("실시간 배출현황", "1", "과종의 등급별 배출현황을 보여줍니다.");
            Menu menu2 = new Menu("Report", "2", "출력물을 출력하실 수 있습니다.");
            Menu menu3 = new Menu("생산자 정보관리", "3", "생산자 정보를 등록/수정할 수 있습니다.");

            _Items.Add(menu1);
            _Items.Add(menu2);
            _Items.Add(menu3);

            return _Items;
        }
    }
}
